# Estimativa para Dev

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)

## Objetivo

Queremos entender como você pensa, como você desenvolveria determinada tarefa e onde você encontra dificuldades ou problemas.

Procure pesar na sua estimativa somente o ideal/necessário para o que foi solicitado.

## O que fazer

1. Analisar o escopo do projeto no link abaixo.
2. Estimar em horas o backend e frontend separados: [https://gitlab.com/lyncas-testes/teste-dev/](https://gitlab.com/lyncas-testes/teste-dev/-/blob/master/README.md).
3. Levantar pontos de dificuldade e atenção que você entende que serão críticos no desenvolvimento.
4. Retornar via Whatsapp com a estimativa para avaliação.
