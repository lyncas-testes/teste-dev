# Teste para Dev

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)

## Como participar?

1. Siga as instruções de desenvolvimento do teste contidas nesse documento.
2. Quando finalizado, compartilhe o código no Git de sua preferência e envie diretamente no contato feito pelo seu Whatsapp.
3. Nosso time fará a análise e te daremos um retorno.

## Detalhes da prova

### Critérios analisados

- Arquitetura do projeto (camadas)
- Funcionalidades e funcionamento
- Código limpo e padrões de projeto (somente o necessário)
- Organização de forma geral

### O que você deve desenvolver

- Uma aplicação para pesquisa de livros e salvar favoritos
- Um README com as instruções para rodar
- Um relatório com o total de horas de desenvolvimento e análise

### Funcionalidades

A API deve conter os seguintes endpoints

1. GET /api/books?p={term} (com paginação), onde term é o termo de pesquisa direto para o Google Books, `ex: ?term=Harry Potter`
2. POST /api/book/{id}/favorite
3. GET /api/book/favorites - retornar uma lista de favoritos
4. DELETE /api/book/{id}/favorite - excluir um favorito

### Especificações técnicas

* A comunicação com o Google Books API deve ser feita utilizando somente a URL `https://www.googleapis.com/books/v1/volumes?q=<filtro>`.
* O Google Books API é aberto e não requer nenhuam autenticação.
* O App deve se comunicar com o Google Books API pelo backend.
